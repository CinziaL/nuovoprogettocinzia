package it.begear.primo.utils;

public class Calcolatrice {
	public static int somma (int primoAddendo, int secondoAddendo)
	{return primoAddendo + secondoAddendo ;
	}
	public static short prodotto(short primoFattore, short secondoFattore)
	{return (short) (primoFattore * secondoFattore) ;
	}
	public static float differenza (float minuendo, float sottraendo)
	{return minuendo - sottraendo;
	}
	public static long secondaDifferenza (long primoMinuendo, long primoSottraendo)
	{return primoMinuendo - primoSottraendo;
	}
}
