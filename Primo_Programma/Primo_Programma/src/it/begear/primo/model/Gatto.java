package it.begear.primo.model;

public class Gatto {
	private String nome;
	private String colore;
	private int eta;
	private boolean sano;
	private boolean salta;
	private double velocita;
	private double velocitaAttuale;
	
	
 
	public Gatto(String nome, String colore, int eta, boolean sano, boolean salta, double velocita,
			double velocitaAttuale) {
		super();
		this.nome = nome;
		this.colore = colore;
		this.eta = eta;
		this.sano = sano;
		this.salta = salta;
		this.velocita = velocita;
		this.velocitaAttuale = velocitaAttuale;
	}

	public void accellera() 
	{if (sano==true )
	{velocitaAttuale = velocitaAttuale +10;
	} else {System.out.println("Il gatto � malato e non pu� muoversi");
	}};
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getColore() {
		return colore;
	}
	public void setColore(String colore) {
		this.colore = colore;
	}
	public int getEta() {
		return eta;
	}
	public void setEta(int eta) {
		this.eta = eta;
	}
	public boolean isSano() {
		return sano;
	}
	public void setSano(boolean sano) {
		this.sano = sano;
	}
	public boolean isSalta() {
		return salta;
	}
	public void setSalta(boolean salta) {
		this.salta = salta;
	}
	public double getVelocita() {
		return velocita;
	}
	public void setVelocita(double velocita) {
		this.velocita = velocita;
	}
	public double getVelocitaAttuale() {
		return velocitaAttuale;
	}
	public void setVelocitaAttuale(double velocitaAttuale) {
		this.velocitaAttuale = velocitaAttuale;
	}
	
	
	

}
