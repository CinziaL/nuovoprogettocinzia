package it.begear.primo.controller;

import java.util.Scanner;

public class Scanner_Manager {
	public static final boolean insboolean = false;

	public static String inserisciStringa1() {
		Scanner scan = new Scanner(System.in);

		if (scan.hasNextLine()) {
			String stringa = scan.nextLine();
			return stringa;
		} else {
			System.out.println("Errore");

		}
		return inserisciStringa1();
	}

	public static int inserisciint() {
		Scanner scan1 = new Scanner(System.in);
		if (scan1.hasNextInt()) {
			int intero = scan1.nextInt();
		} else {
			System.out.println("Errore: inserire l'et� del gatto in anni !");
		}
		return 0;
	}

	public static boolean insboo() {
		Scanner scan2 = new Scanner(System.in);
		if (scan2.hasNextBoolean()) {
			boolean booleano = scan2.nextBoolean();
		} else {
			System.out.println("Inserire true se il gatto gode di buona salute e false altrimenti");
		}
		return false;
	}

	public static double insd() {
		Scanner scan3 = new Scanner(System.in);
		if (scan3.hasNextDouble()) {
			double dou = scan3.nextDouble();
		} else {
			System.out.println("Inserire la velocit� in km orari");
		}
		return 0;
	}
}